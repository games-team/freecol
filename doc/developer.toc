\contentsline {chapter}{\numberline {1}How to become a FreeCol developer}{5}{chapter.1}%
\contentsline {section}{\numberline {1.1}The goal of our project}{5}{section.1.1}%
\contentsline {section}{\numberline {1.2}SourceForge project site}{5}{section.1.2}%
\contentsline {section}{\numberline {1.3}How to find tasks}{6}{section.1.3}%
\contentsline {section}{\numberline {1.4}How to use the trackers}{6}{section.1.4}%
\contentsline {section}{\numberline {1.5}Mailing lists}{8}{section.1.5}%
\contentsline {section}{\numberline {1.6}Git}{8}{section.1.6}%
\contentsline {section}{\numberline {1.7}Compiling the code}{8}{section.1.7}%
\contentsline {section}{\numberline {1.8}Using an IDE}{9}{section.1.8}%
\contentsline {subsection}{\numberline {1.8.1}Using Eclipse (thanks to ``nobody'')}{9}{subsection.1.8.1}%
\contentsline {subsection}{\numberline {1.8.2}Using NetBeans (thanks to ``wintertime'')}{11}{subsection.1.8.2}%
\contentsline {subsection}{\numberline {1.8.3}Creating a new NetBeans project (thanks to ``xsainnz'')}{11}{subsection.1.8.3}%
\contentsline {section}{\numberline {1.9}Code documentation}{12}{section.1.9}%
\contentsline {section}{\numberline {1.10}Code Quality}{12}{section.1.10}%
\contentsline {chapter}{\numberline {2}How to make a FreeCol release}{13}{chapter.2}%
\contentsline {chapter}{\numberline {3}Missing features}{17}{chapter.3}%
\contentsline {chapter}{\numberline {4}Changing the Rules}{19}{chapter.4}%
\contentsline {section}{\numberline {4.1}Modifiers and Abilities}{20}{section.4.1}%
\contentsline {chapter}{\numberline {5}Mods}{33}{chapter.5}%
\contentsline {chapter}{\numberline {6}Resources}{35}{chapter.6}%
\contentsline {chapter}{\numberline {7}Map Format}{37}{chapter.7}%
